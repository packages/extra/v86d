# Maintainer: Philip Müller <philm at manjaro dot org>

pkgname=v86d
pkgver=0.1.10
pkgrel=9
pkgdesc="Userspace helper for uvesafb that runs x86 code in an emulated environment"
arch=('x86_64')
url="https://github.com/mjanusz/v86d"
license=('GPL-2.0-or-later')
depends=('glibc')
makedepends=('git')
source=("git+https://github.com/mjanusz/v86d.git#tag=$pkgname-$pkgver"
        "${pkgname}_install"
        "${pkgname}_hook"
        'modprobe.uvesafb')
sha256sums=('a7c7830de59f72ad625779cce4dbbe6dc5260f3fec8a39c522f1d6743caff0e6'
            'b33de32a20303dbae9a4ef20636852170d740afb832249903ff37e7454311663'
            'e1c05aabfb25d40de51555865286b22a5511ff1dc85cb7af0ab1baa896e32dd5'
            '5d5949ec23a546d1468327e5496e8cc2b0b2015b84ff8bedb6d0b462df59bd19')

build() {
  export CFLAGS+=' -Wno-error=implicit-function-declaration'
  export CXXFLAGS+=' -Wno-error=implicit-function-declaration'

  cd "$pkgname"
  ./configure --with-x86emu

  # we only need /usr/include/video/uvesafb.h
  make KDIR=/usr
}

package() {
  cd "$pkgname"
  make DESTDIR="$pkgdir" install

  install -D -m644 "$srcdir/${pkgname}_install" "$pkgdir/usr/lib/initcpio/install/$pkgname"
  install -D -m644 "$srcdir/${pkgname}_hook" "$pkgdir/usr/lib/initcpio/hooks/$pkgname"
  install -D -m644 "$srcdir/modprobe.uvesafb" "$pkgdir/usr/lib/modprobe.d/uvesafb.conf"
}
